#!/bin/bash

sudo pip install -r scripts/requirements.txt
echo "*******Installing AWS Inspector Agent********"
curl -o "/tmp/inspector_install" https://inspector-agent.amazonaws.com/linux/latest/install
sudo bash /tmp/inspector_install
sudo /etc/init.d/awsagent stop
rm -rf /tmp/inspector_install