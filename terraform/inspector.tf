 resource "aws_inspector_resource_group" "inspector" {
   tags = {
     inspector = "true"
   }
 }

 resource "aws_inspector_assessment_target" "inspector" {
   name               = "tf-inspector-demo"
   resource_group_arn = "${aws_inspector_resource_group.inspector.arn}"
 }

 resource "aws_inspector_assessment_template" "inspector" {
   name       = "tf-inspector-demo"
   target_arn = "${aws_inspector_assessment_target.inspector.arn}"
   duration   = 960

   rules_package_arns = [
     "arn:aws:inspector:us-east-1:316112463485:rulespackage/0-gEjTy7T7",
     "arn:aws:inspector:us-east-1:316112463485:rulespackage/0-rExsr2X8",
     "arn:aws:inspector:us-east-1:316112463485:rulespackage/0-PmNV0Tcd",
     "arn:aws:inspector:us-east-1:316112463485:rulespackage/0-R01qwB5Q",
   ]
 }
