provider "aws" {
  region = "us-east-1"
  
}
resource "aws_instance" "inspector-instance" {
  key_name      = "user1"
  ami           = var.image_id
  instance_type = "t2.micro"

  tags = {
    Name = "InspectInstances"
    inspector = "true",
    SSM       = "update"
    
  }

  vpc_security_group_ids = ["sg-0bff427d0f7e1510e"]
  subnet_id              = "subnet-59ddc857"
  user_data = "${file("user-data-inpector.sh")}"
}
