output "instance_id" {
  description = "ID of the EC2 instance"
  value       = aws_instance.inspector-instance.id  
}
output "inspector_id" {
  description = "ID of the EC2 instance"
  value       = aws_inspector_assessment_template.inspector.id
}
